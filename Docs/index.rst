.. Microbial Bioinformatics Hub documentation master file, created by
   sphinx-quickstart on Thu Jun 17 12:58:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Microbial Bioinformatics Hub!
========================================================

Microbial Bioinformatics Hub an open-source, collaborative space to find, learn and share  knowledge, methods and tools related to analyzing microbiological data. The major aim of MBH is to improve accessibility of microbial bioinformatics. To do that, we've crowdsourced tutorials and resources, and made some of our own. You'll find these in the links below:

.. note::

	This site is currently under construction.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   gettingstarted
   Microbiome/microbiome
   Troubleshooting/troubleshooting

.. toctree::
   :maxdepth: 1
   :caption: Coming soon:

   Statistics/statistics
   R/r_basics
   Git/git_basics

.. toctree::
   :hidden:

   Contributors/contributors
   Test/test
