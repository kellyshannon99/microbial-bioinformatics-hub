Test
====

This document is to test Sphinx features.


Multiple difficulty levels
--------------------------

.. tabbed:: Basic

    .. code-block:: R

        ggplot(df, aes(x = group, y = values)) +
            geom_boxplot()

.. tabbed:: Intermediate

    .. code-block:: R

        df %>%
            ggplot(aes(x = group, y = values)) +
                geom_boxplot()

.. tabbed:: Advanced

    .. code-block:: R

        df %>%
            ggplot(aes(x = group, y = values)) +
                geom_boxplot(aes(fill = group), alpha = .75) +
                geom_jitter(aes(color = group)) +
                facet_grid(Row_facet ~ Column_facet, scales = "free_y") +
                stat_compare_means(method = "test_type", method.args=list(alternative = "two.sided"), label.y = .95) +
                scale_fill_manual(name = "Name of variable", values = c(custom.color.palette)) +
                scale_color_manual(name = "Name of variable", values = c(custom.color.palette), guide = F)  +
                labs(title = "A title goes here", caption = "A caption goes here", x = "x axis label", y = "y axis label")
