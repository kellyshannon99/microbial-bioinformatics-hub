Troubleshooting
===============

.. warning::

	This page is currently under construction.

In an ideal world, running code would be as easy as the push of a button. Often, that's not the case because of the nature of bioinformatics. More often than not, because of your unique dataset or experimental design, it requires you to modify code to suit your individual requirements. This can result in errors. Overcoming errors in code is a unique skillset that is unfortunately honed through trial and error. Hopefully, this tutorial can minimize the pain and suffering that comes with debugging your errors by providing an efficient system for discovering the root of your issues or what to do if you cannot resolve them.



1. Go to the GitHub issues page associated with the tool you're using. In this case, we're interested in `DADA2's GitHub issues page <https://github.com/benjjneb/dada2/issues>`_.
2. Next, go to the search bar and delete all by `is:issue`. It should look like this:

.. image:: /../Media/Images/Troubleshooting/Github-issues-search-bar.png
   :align: center

3. There's no perfect recipe for what text to include in your search, but generally the first thing you'll do is copy the entire error message the software printed to the screen.

   * Often the error will include information specific to you like a directly path name (e.g. "`/User/Project/Path/Name/file.abc`"). These details can be omitted.
      .. (include example)

   * If that doesn't work, try some keywords from the error message along with the main step (e.g. "error filter trim `<key words>`")
      .. (include example)

      * This makes your search more general and you might be able to find some issues related to your specific problem.

   * If you aren't having any luck your next step is to broaden your search by going to a Google and repeating the previous steps but using Google's search engine.

   * Finally, your next best option is to ask for help.

      * Find someone more experienced with the tool you're using or submit an issue to the GitHub page. Either method you choose, we highly recommend you follow this advice for submitting an issue or asking someone for help in order to provide sufficient context so that someone can most efficiently help you.
