# Welcome to Microbial Bioinformatics Hub GitLab Page

Microbial Bioinformatics Hub is an open-source, collaborative space for researchers and students to find, learn and share knowledge, methods and tools related to analyzing microbiological data. You can find the site [here](https://microbial-bioinformatics-hub.readthedocs.io/en/latest/index.html).


If you find an error please let us know [here](https://gitlab.com/sielerjm/microbial-bioinformatics-hub/-/issues) and if you’d like to contribute please get in touch with us [here](mailto:sielerjm(at)oregonstate(dot)com>). Thanks!


<div align="center"><img src="https://gitlab.com/sielerjm/microbial-bioinformatics-hub/uploads/250eda5549be43c73f2fcf73bc5fd555/MBH_logo_v2_200px_.png" width="150"
     height="150"/></div>
